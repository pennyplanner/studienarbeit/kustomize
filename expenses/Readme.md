# Expenses
## install expenses
```shell
kubectl create namespace expenses
kubectl config set-context --current --namespace expenses
# kubectl apply -k (an diesem Punkt hier kommt der Verzeichnispfad rein) . --prune -l app.kubernetes.io/part-of=expenses -n expenses
kubectl apply -k . --prune -l app.kubernetes.io/part-of=expenses -n expenses
kubectl wait pods -n expenses -l app.kubernetes.io/part-of=expenses --for condition=Ready --timeout=120s
```
