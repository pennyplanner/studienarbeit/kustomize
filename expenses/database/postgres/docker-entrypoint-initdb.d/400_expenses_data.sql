\c expenses;
DO
$$
    DECLARE
        categoryid categories.id%TYPE;
    BEGIN
        INSERT INTO categories (name)
        VALUES ('income')
        RETURNING id INTO categoryid;
        INSERT INTO expenses (category_id, value, date, note, username)
        VALUES (categoryid, 1200, '2023-10-01', 'income October 2023', 'biletado'),
               (categoryid, 1200, '2023-11-01', 'income November 2023', 'biletado');
        INSERT INTO categories (name)
        VALUES ('groceries')
        RETURNING id INTO categoryid;
        INSERT INTO expenses (category_id, value, date, note, username)
        VALUES (categoryid, 5.22, '2023-10-15', 'two big bananas', 'biletado'),
               (categoryid, 17.20, '2023-10-22', 'supermarket', 'biletado');
    END
$$ LANGUAGE plpgsql;
