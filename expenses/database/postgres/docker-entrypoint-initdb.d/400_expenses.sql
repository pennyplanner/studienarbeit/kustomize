CREATE
    DATABASE expenses;
\c expenses;
CREATE
    EXTENSION IF NOT EXISTS "uuid-ossp";


create table if not exists categories
(
    id         uuid default uuid_generate_v4() not null
        primary key,
    "name"     text                            not null,
    deleted_at timestamptz
);

alter table categories
    owner to postgres;

create table if not exists expenses
(
    id          uuid default uuid_generate_v4() not null
        primary key,
    category_id uuid                            not null
        references categories,
    "value"     numeric(11,4)                   not null,
    date        date                            not null,
    note        text,
    username    varchar(255)                    not null,
    deleted_at  timestamptz
);

alter table expenses
    owner to postgres;
